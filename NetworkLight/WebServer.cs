using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using System.Text;

namespace NetworkLight
{
    class WebServer : IDisposable
    {
        private Socket socket = null;
        private bool ledState = false;
        private OutputPort led = new OutputPort(Pins.GPIO_PIN_D9, false);

        public WebServer()
        {
            //IPAddress netdurinoIP = IPAddress.Parse("192.168.1.103");

            //Init the socket
            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.socket.Bind(new IPEndPoint(IPAddress.Any, 3003));

            //Print out the server IP
            Debug.Print(Microsoft.SPOT.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()[0].IPAddress);

            //Listen out for requests...
            socket.Listen(1);
        }

        //Method: handleNetRequests - listens for incoming requests, parses them and deals with them.
        public void handleNetRequests()
        {
            while (true)
            {
                using(Socket clientSocket = socket.Accept())
                {
                    //Grab the client's IP Address... 
                    IPEndPoint clientIP = clientSocket.RemoteEndPoint as IPEndPoint;
                    EndPoint clientEndPoint = clientSocket.RemoteEndPoint;

                    int bytesReceived = clientSocket.Available;

                    if (bytesReceived > 0)
                    {
                        //Get the request...
                        byte[] buffer = new byte[bytesReceived];
                        int byteCount = clientSocket.Receive(buffer, bytesReceived, SocketFlags.None);

                        //Get and dump the request to the debug...
                        string request = new string(Encoding.UTF8.GetChars(buffer));
                        Debug.Print(request);

                        //toggle the led...
                        string ledTxtState = "";
                        if (ledState)
                        {
                            ledState = false;
                            ledTxtState = "off";
                        }
                        else
                        {
                            ledState = true;
                            ledTxtState = "on";
                        }

                        


                        //Send out a response
                        
                        string response = "The LED is: " + ledTxtState;
                        string header = "HTTP/1.0 200 OK\r\nContent-Type: text; charset=utf-8\r\nContent-Length: " + response.Length.ToString() + "\r\nConnection: close\r\n\r\n";

                        clientSocket.Send(Encoding.UTF8.GetBytes(header), header.Length, SocketFlags.None);
                        clientSocket.Send(Encoding.UTF8.GetBytes(response), response.Length, SocketFlags.None);

                        led.Write(ledState);
                        

                    }
                }
            }
        }

        ~WebServer()
        {
            Dispose();
        }

        //When destroying the webserver class, make sure we close and nullify the socket member.
        public void Dispose()
        {
            if (this.socket != null)
            {
                try
                {
                    this.socket.Close();
                }
                catch (Exception e)
                {
                    //do nothing for now....
                }
                finally
                {
                    this.socket = null;
                }
            }
        }


    }
}
